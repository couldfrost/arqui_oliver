import SalesTable from "../src/domain/utils/generatePaymentSlipLogic/Tabla/TablaVentas";

let expect = require('chai').expect;
import CalcularSalarioFijo from '../src/domain/utils/generatePaymentSlipLogic/CalculadoraSalario/CalculadoraSalarioFijo.js';
import CalcularSalarioHoras from '../src/domain/utils/generatePaymentSlipLogic/CalculadoraSalario/CalculadoraSalarioHoras.js';
import CalcularSalarioComision from '../src/domain/utils/generatePaymentSlipLogic/CalculadoraSalario/CalculadoraSalarioComision.js';
import ScheduleTable from '../src/domain/utils/generatePaymentSlipLogic/Tabla/TablaHorarios.js';
import Empleado from '../src/domain/utils/generatePaymentSlipLogic/Empleado/Empleado.js';
import GeneradorBoletas from "../src/domain/utils/generatePaymentSlipLogic/Boleta/GeneradorBoletas";

describe('Generador de boletas', function() {

    it('Deberia retornar la boleta del empleado', function() {
        let calcularSalario = new CalcularSalarioFijo(1000);
        let empleado = new Empleado('Jorge', 20, 'Fijo', calcularSalario);
        let generador = new GeneradorBoletas();
        expect(generador.generarBoleta(empleado)).equal('Nombre: Jorge\nCi: 20\nSueldo: 1000');
    });

    it('Deberia retornar la boleta del empleado', function() {
        let calcularSalario = new CalcularSalarioFijo(2000);
        let empleado = new Empleado('Alvaro', 20, 'Fijo', calcularSalario);
        let generador = new GeneradorBoletas();
        expect(generador.generarBoleta(empleado)).equal('Nombre: Alvaro\nCi: 20\nSueldo: 2000');
    });
    it('Deberia retornar las boletas de los empleados', function() {
        let listaEmpleado=[];
        let calcularSalario1 = new CalcularSalarioFijo(2000,'2019-04-01');
        let calcularSalario2= new CalcularSalarioFijo(2000,'2019-04-01');
        let empleado1 = new Empleado('Alvaro', 20, 'Fijo', calcularSalario1);
        let empleado2 = new Empleado('juan', 20, 'Fijo', calcularSalario2);
        listaEmpleado.push(empleado1);
        listaEmpleado.push(empleado2);
        let generador = new GeneradorBoletas();
        expect(generador.generarBoletas(listaEmpleado)).equal('Nombre: Alvaro\nCi: 20\nSueldo: 2000,Nombre: juan\nCi: 20\nSueldo: 2000');
    });
    it('Deberia retornar las boletas de los empleados', function() {
        let listaEmpleado=[];
        let calcularSalario1 = new CalcularSalarioFijo(2000,'2019-04-01');
        let calcularSalario2= new CalcularSalarioFijo(2000,'2019-04-01');
        let empleado1 = new Empleado('Alvaro', 20, 'Fijo', calcularSalario1);
        let empleado2 = new Empleado('juan', 21, 'Fijo', calcularSalario2);
        let schedule = new ScheduleTable([
            ['2019-05-21 14:00:00', '2019-05-21 16:00:00'],
            ['2019-05-21 18:00:00', '2019-05-21 21:00:00']
        ]);
        let calcularSalario3 = new CalcularSalarioHoras(200,schedule);
        let empleado3 = new Empleado('pancho', 22, 'Horas',calcularSalario3);
        let sales = new SalesTable([
            ['2019-05-21 14:00:00', 2000],
            ['2019-05-21 15:00:00', 4000]
        ]);
        let calcularSalario4 = new CalcularSalarioComision(1000,0.5,sales);
        let empleado4 = new Empleado('lili', 1, 'Comision', calcularSalario4);
        listaEmpleado.push(empleado1);
        listaEmpleado.push(empleado2);
        listaEmpleado.push(empleado3);
        listaEmpleado.push(empleado4);
        let generador = new GeneradorBoletas();
        expect(generador.generarBoletas(listaEmpleado)).equal('Nombre: Alvaro\nCi: 20\nSueldo: 2000,Nombre: juan\nCi: 21\nSueldo: 2000,Nombre: pancho\nCi: 22\nSueldo: 1000,Nombre: lili\nCi: 1\nSueldo: 4000');
    });

});
