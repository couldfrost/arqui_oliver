
let expect = require('chai').expect;

import Empleado from '../src/domain/utils/generatePaymentSlipLogic/Empleado/Empleado.js';


describe('Empleado', function() {

    it('Deberia retornar el nombre del empleado', function() {
        let empleado = new Empleado('Jorge');
        expect(empleado.obtenerNombre()).equal('Jorge');
    });

    it('Deberia retornar el nombre del empleado', function() {
        let empleado = new Empleado('Alvaro');
        expect(empleado.obtenerNombre()).equal('Alvaro');
    });

    it('Deberia retornar el ci del empleado', function() {
        let empleado = new Empleado('Jorge', 10);
        expect(empleado.obtenerCi()).equal(10);
    });

    it('Deberia retornar el ci del segundo empleado', function() {
        let empleado = new Empleado('Jorge', 20);
        expect(empleado.obtenerCi()).equal(20);
    });
    
});
