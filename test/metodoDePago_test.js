let expect = require('chai').expect;
import CalcularSalarioFijo from '../src/domain/utils/generatePaymentSlipLogic/CalculadoraSalario/CalculadoraSalarioFijo.js';
import CalcularSalarioHoras from '../src/domain/utils/generatePaymentSlipLogic/CalculadoraSalario/CalculadoraSalarioHoras.js';
import CalcularSalarioComision from '../src/domain/utils/generatePaymentSlipLogic/CalculadoraSalario/CalculadoraSalarioComision.js';
import MetodoCheque from "../src/domain/utils/generatePaymentSlipLogic/MetodoDePago/MetodoCheque";
import MetodoDeposito from "../src/domain/utils/generatePaymentSlipLogic/MetodoDePago/MetodoDeposito";
import MetodoRRHH from "../src/domain/utils/generatePaymentSlipLogic/MetodoDePago/MetodoRRHH";
import Empleado from '../src/domain/utils/generatePaymentSlipLogic/Empleado/Empleado.js';

describe('Metodo de pago', function() {
    
        it('Deberia retornar el cheque del empleado', function() {
            let calcularSalario = new CalcularSalarioFijo(2000,'2019-04-01');
            let empleado = new Empleado('Alvaro', 20, 'Fijo',calcularSalario,null,"Cheque");
            let metododepago = new MetodoCheque(empleado,"2019-4-14");
            expect(metododepago.generarMetodoDePago()).equal('Cheque\nFecha: 14-4-2019\nPagase: Alvaro\nMonto: 2000');
        });
        it('Deberia retornar el recibo del empleado', function() {
            let calcularSalario = new CalcularSalarioFijo(2000,'2019-04-01');
            let empleado = new Empleado('Alvaro', 20, 'Fijo',calcularSalario,null,"RRHH");
            let metododepago = new MetodoRRHH(empleado,5000,"2019-5-14");
            expect(metododepago.generarMetodoDePago()).equal('Recibo\nFecha: 14-5-2019\nRecibido: Alvaro\nMonto: 2000');
        });
    });