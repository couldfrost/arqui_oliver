
let expect = require('chai').expect;

import Cliente from '../src/domain/utils/generatePaymentSlipLogic/MetodoDePago/Cliente.js';

describe('Cliente', function() {
    
        it('Deberia darme el monto del cliente', function() {
            let cliente = new Cliente(12,'Jorge',0);
            expect(cliente.obtenerMonto()).equal(0);
        });
        it('Deberia darme el monto del cliente', function() {
            let cliente = new Cliente(12,'Jorge',0);
            cliente.cambiarMonto(200);
            expect(cliente.obtenerMonto()).equal(200);
        });
        
    });
    