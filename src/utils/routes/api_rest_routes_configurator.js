const { Router } = require('express');
const path = require('path');
const APIRESTController = require(
    path.join(process.cwd(), 'src', 'presentation', 'apiRest', 'controller', 'api_rest_controller')
);
const TablaHorarios = require(
    path.join(process.cwd(), 'src', 'domain', 'utils', 'generatePaymentSlipLogic', 'Tabla', 'TablaHorarios')
);
const TablaVentas = require(
    path.join(process.cwd(), 'src', 'domain', 'utils', 'generatePaymentSlipLogic', 'Tabla', 'TablaVentas')
);
const CalculadoraSalarioComision = require(
    path.join(process.cwd(), 'src', 'domain', 'utils', 'generatePaymentSlipLogic', 'CalculadoraSalario', 'CalculadoraSalarioComision')
);
const CalculadoraSalarioFijo = require(
    path.join(process.cwd(), 'src', 'domain', 'utils', 'generatePaymentSlipLogic', 'CalculadoraSalario', 'CalculadoraSalarioFijo')
);
const CalculadoraSalarioHoras = require(
    path.join(process.cwd(), 'src', 'domain', 'utils', 'generatePaymentSlipLogic', 'CalculadoraSalario', 'CalculadoraSalarioHoras')
);
const Empleado = require(
    path.join(process.cwd(), 'src', 'domain', 'utils', 'generatePaymentSlipLogic', 'Empleado', 'Empleado')
);

const router = new Router();

router.post('/registerEmployeeUseCase', async (req, res) => {
    console.log('Register Employee Use Case Start');
    let apiRestController = new APIRESTController(res);

    await apiRestController.registerEmployeeUseCase(req.body);

    console.log('Register Employee Use Case End');
});

router.get('/getEmployeesUseCase/:type', async (req, res) => {
    console.log('Get Employees Use Case Start');
    let apiRestController = new APIRESTController(res);

    await apiRestController.getEmployeesUseCase(req.params);

    console.log('Get Employees Use Case End');
});

router.post('/generatePaymentSlipUseCase', async (req, res) => {
    console.log('Generate Payment Slip Use Case Start');

    let type = req.body.type;
    let calculadoraSalario = null;

    if (type == 'C') {
        let salario = 1100;
        let porcentaje = 0.5;
        let tablaVentas = new TablaVentas([
            ['2019-05-21 14:00:00', 2000],
            ['2019-05-21 15:00:00', 4000]
        ]);
        calculadoraSalario = new CalculadoraSalarioComision(salario, porcentaje, tablaVentas);
    } else if (type == 'F') {
        let salario = 2200;
        calculadoraSalario = new CalculadoraSalarioFijo(salario);
    } else if (type == 'H') {
        let salario = 400;
        let tablaHorarios = new TablaHorarios([
            ['2019-05-21 10:00:00', '2019-05-21 22:00:00']
        ]);
        calculadoraSalario = new CalculadoraSalarioHoras(salario, tablaHorarios);
    }

    let nombre = req.body.name;
    let ci = req.body.id;
    let tipo = req.body.type;

    let empleado = new Empleado(nombre, ci, tipo, calculadoraSalario);

    let apiRestController = new APIRESTController(res);

    await apiRestController.generatePaymentSlipUseCase(empleado);

    console.log('Generate Payment Slip Use Case End');
});

module.exports = router;