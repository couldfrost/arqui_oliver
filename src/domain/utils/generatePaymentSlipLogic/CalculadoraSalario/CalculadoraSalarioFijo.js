class CalculadoraSalarioFijo {
    
        constructor(monto,fechaInicio) {
            this.monto= monto;
            this.fechaInicio = fechaInicio;
        }
    
        obtenerSalario() {
            let fecha = new Date(this.fechaInicio);
            let fechaActual = new Date();
            if(fechaActual.getMonth() === fecha.getMonth() && fecha.getFullYear() === fechaActual.getFullYear()) {
                let ultimaFechaDelMes = new Date(fechaActual.getFullYear(), fechaActual.getMonth() + 1, 0);
                let descuento = (ultimaFechaDelMes.getDate() - fecha.getDate()) / ultimaFechaDelMes.getDate();
                return this.monto * descuento;
            }
            if(fecha > fechaActual) {
                return 0;
            }
            return this.monto;
        }
    }
    
    module.exports = CalculadoraSalarioFijo;
    
