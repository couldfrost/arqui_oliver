class CalculadoraSalarioHoras {

    constructor(monto,horarios) {
        this.monto = monto;
        this.horarios = horarios;
    }

    obtenerSalario() {
        return this.monto*(this.horarios.obtenerHoras() + this.horarios.obtenerHorasExtra() * 1.5);
    }
}

module.exports = CalculadoraSalarioHoras;
