class CalculadoraSalarioComision {

    constructor(monto,porcentaje,ventas) {
        this.monto = monto;
        this.porcentaje = porcentaje;
        this.ventas = ventas;
    }

    obtenerSalario() {
        return this.monto + this.porcentaje * this.ventas.ObtenerDinero();
    }
}

module.exports = CalculadoraSalarioComision;
