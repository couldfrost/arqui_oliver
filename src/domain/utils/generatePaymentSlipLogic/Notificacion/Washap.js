class Washap {
    constructor(correoelectronico) {
        this.correoelectronico=correoelectronico;
    }
    generarNotificacion(){
        return this.correoelectronico.generarBoleta()+ ",Washap";
    }
}

module.exports = Washap;