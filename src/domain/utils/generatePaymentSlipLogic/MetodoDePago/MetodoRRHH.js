class MetodoRRHH {
    
            constructor(empleado,cajaChica,fecha) {
                this.nombre=empleado.nombre;
                this.sueldo=empleado.obtenerSalario();
                this.cajaChica=cajaChica; 
                this.fecha=new Date(fecha);
            }
            pagadoPorRRHH(){
                return this.cajaChica=this.cajaChica-this.sueldo;
            }

            generarRecibo() {
                return `Recibo\nFecha: ${this.fecha.getDate() + '-' + (this.fecha.getMonth()+1) + '-' + this.fecha.getFullYear()}\nRecibido: ${this.nombre}\nMonto: ${this.sueldo}`;
            }
            generarMetodoDePago(){
                return this.generarRecibo();
                
            }
            
}
module.exports = MetodoRRHH;