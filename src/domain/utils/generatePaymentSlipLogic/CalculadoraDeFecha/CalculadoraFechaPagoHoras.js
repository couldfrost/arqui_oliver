class CalculadoraFechaPagoHoras {
    constructor(fechaInicio) {
        this.fechaInicial = fechaInicio;
    }

    inicioDePago() {
        let ultimoFechaDeLaSemana = new Date(this.fechaInicial.getFullYear(), this.fechaInicial.getMonth(), this.fechaInicial.getDate());
        while(ultimoFechaDeLaSemana.getDay() != 5) {
            ultimoFechaDeLaSemana.setDate(ultimoFechaDeLaSemana.getDate() + 1);
        }
        return ultimoFechaDeLaSemana.getDate() + '-' + (ultimoFechaDeLaSemana.getMonth() + 1) + '-' + ultimoFechaDeLaSemana.getFullYear();
    }
}

module.exports = CalculadoraFechaPagoHoras;