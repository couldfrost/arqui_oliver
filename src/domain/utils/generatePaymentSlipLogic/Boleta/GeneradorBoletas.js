class GeneradorBoletas {
    constructor() {

    }

    generarBoleta(empleado) {
        return `Nombre: ${empleado.nombre}\nCi: ${empleado.ci}\nSueldo: ${empleado.obtenerSalario()}`;
    }

    generarBoletas(listaEmpleados){
         let listaDeBoletas=[];
         for(const empleado of listaEmpleados){
             listaDeBoletas.push(this.generarBoleta(empleado));
         }        
         return listaDeBoletas.toString();
    }
}

module.exports = GeneradorBoletas;
