class PaymentSlip {
    constructor(employeeId, employeeName, employeeType, salary) {
        this._employeeId = employeeId;
        this._employeeName = employeeName;
        this._employeeType = employeeType;
        this._salary = salary;
    }

    get employeeId() { return this._employeeId }
    get employeeName() { return this._employeeName; }
    get employeeType() { return this._employeeType; }
    get salary() { return this._salary; }
}

module.exports = PaymentSlip;