class GeneratePaymentSlipRequest {
    constructor(employee) {
        this._employee = employee;
    }

    get employee() { return this._employee; }
}

module.exports = GeneratePaymentSlipRequest;