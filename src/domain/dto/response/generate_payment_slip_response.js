class GeneratePaymentSlipResponse {
    constructor(paymentSlip) {
        this._paymentSlip = paymentSlip;
    }

    get paymentSlip() { return this._paymentSlip; }
}

module.exports = GeneratePaymentSlipResponse;