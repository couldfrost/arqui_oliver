const path = require('path');
const GeneratePaymentSlipUseCase = require(
    path.join(process.cwd(), 'src', 'domain', 'boundary', 'input', 'generate_payment_slip_use_case')
);
const PaymentSlip = require(
    path.join(process.cwd(), 'src', 'domain', 'entities', 'payment_slip')
);
const GeneratePaymentSlipResponse = require(
    path.join(process.cwd(), 'src', 'domain', 'dto', 'response', 'generate_payment_slip_response')
);

class GeneratePaymentSlipInteractor extends GeneratePaymentSlipUseCase {
    constructor(paymentSlipRepository, generatePaymentSlipPresenter) {
        super();
        this._paymentSlipRepository = paymentSlipRepository;
        this._generatePaymentSlipPresenter = generatePaymentSlipPresenter;
    }

    async generatePaymentSlip(request) {
        let employee = request.employee;
        let paymentSlip = new PaymentSlip(employee.obtenerCi(), employee.obtenerNombre(), employee.obtenerTipo(), employee.obtenerSalario());
        await this._paymentSlipRepository.savePaymentSlip(paymentSlip);

        let generatePaymentSlipResponse = new GeneratePaymentSlipResponse(paymentSlip);
        this._generatePaymentSlipPresenter.displayGeneratedPaymentSlip(generatePaymentSlipResponse);
    }
}

module.exports = GeneratePaymentSlipInteractor;