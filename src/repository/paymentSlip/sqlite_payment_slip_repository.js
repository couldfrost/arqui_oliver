const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const PaymentSlipRepository = require(
    path.join(process.cwd(), 'src', 'domain', 'entityGateways', 'payment_slip_repository')
);
const PaymentSlip = require(
    path.join(process.cwd(), 'src', 'domain', 'entities', 'payment_slip')
);

class SQLitePaymentSlipRepository extends PaymentSlipRepository {
    constructor() {
        super();
    }

    async savePaymentSlip(paymentSlip) {
        let executionInsertPaymentSlipPromise = new Promise((resolve, reject) => {
            let db = new sqlite3.Database(path.join(process.cwd(), 'db', 'companydb.db'), sqlite3.OPEN_READWRITE, (err) => {
                if (err) {
                    console.error(`ERROR IN OPEN DATABASE: ${err.message}`);
                    process.exit(1);
                }
            });

            let insertPaymentSlipStatement = `INSERT INTO PaymentSlip (
                                                employeeId,
                                                employeeName,
                                                employeeType,
                                                salary
                                        ) VALUES (
                                                '${paymentSlip.employeeId}',
                                                '${paymentSlip.employeeName}',
                                                '${paymentSlip.employeeType}',
                                                '${paymentSlip.salary}'
                                        )`;

            db.run(insertPaymentSlipStatement, [], (err) => {
            if (err) {
                console.error(`ERROR IN INSERT STATEMENT: ${err.message}`);
                process.exit(1);
            }
                resolve(true);
            });

            db.close();

        });

        await executionInsertPaymentSlipPromise;
    }
}

module.exports = SQLitePaymentSlipRepository;