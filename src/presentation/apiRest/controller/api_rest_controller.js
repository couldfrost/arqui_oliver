const path = require('path');
const APIRESTRegisterEmployeeView = require(
    path.join(process.cwd(), 'src', 'presentation', 'apiRest', 'view', 'api_rest_register_employee_view')
);
const APIRESTGetEmployeesView = require(
    path.join(process.cwd(), 'src', 'presentation', 'apiRest', 'view', 'api_rest_get_employees_view')
);
const APIRESTGeneratePaymentSlipView = require(
    path.join(process.cwd(), 'src', 'presentation', 'apiRest', 'view', 'api_rest_generate_payment_slip_view')
);
const APIRESTRegisterEmployeePresenter = require(
    path.join(process.cwd(), 'src', 'presentation', 'apiRest', 'presenter', 'api_rest_register_employee_presenter')
);
const APIRESTGetEmployeesPresenter = require(
    path.join(process.cwd(), 'src', 'presentation', 'apiRest', 'presenter', 'api_rest_get_employees_presenter')
);
const APIRESTGeneratePaymentSlipPresenter = require(
    path.join(process.cwd(), 'src', 'presentation', 'apiRest', 'presenter', 'api_rest_generate_payment_slip_presenter')
);
const SQLiteEmployeeRepository = require(
    path.join(process.cwd(), 'src', 'repository', 'employee', 'sqlite_employee_repository')
);
const SQLitePaymentSlipRepository = require(
    path.join(process.cwd(), 'src', 'repository', 'paymentSlip', 'sqlite_payment_slip_repository')
);
const RegisterEmployeeRequest = require(
    path.join(process.cwd(), 'src', 'domain', 'dto', 'request', 'register_employee_request')
);
const GetEmployeesRequest = require(
    path.join(process.cwd(), 'src', 'domain', 'dto', 'request', 'get_employees_request')
);
const GeneratePaymentSlipRequest = require(
    path.join(process.cwd(), 'src', 'domain', 'dto', 'request', 'generate_payment_slip_request')
);
const RegisterEmployeeInteractor = require(
    path.join(process.cwd(), 'src', 'domain', 'interactors', 'register_employee_interactor')
);
const GetEmployeesInteractor = require(
    path.join(process.cwd(), 'src', 'domain', 'interactors', 'get_employees_interactor')
);
const GeneratePaymentSlipInteractor = require(
    path.join(process.cwd(), 'src', 'domain', 'interactors', 'generate_payment_slip_interactor')
);

class APIRESTController {
    constructor(serverResponse) {
        this._serverResponse = serverResponse;
    }

    async registerEmployeeUseCase(serverRequestBody) {
        let id = serverRequestBody.id;
        let name = serverRequestBody.name;
        let type = serverRequestBody.type;
        let isInLaborUnion = serverRequestBody.isInLaborUnion;


        let registerEmployeeView = new APIRESTRegisterEmployeeView(this._serverResponse);
        let registerEmployeePresenter = new APIRESTRegisterEmployeePresenter(registerEmployeeView);
        let employeeRepository = new SQLiteEmployeeRepository();
        let registerEmployeeRequest = new RegisterEmployeeRequest(id, name, type, isInLaborUnion);
        let registerEmployeeUseCase = new RegisterEmployeeInteractor(employeeRepository, registerEmployeePresenter);

        await registerEmployeeUseCase.registerEmployee(registerEmployeeRequest);
    }

    async getEmployeesUseCase(serverRequestParams) {
        let type = serverRequestParams.type;

        let getEmployeesView = new APIRESTGetEmployeesView(this._serverResponse);
        let getEmployeesPresenter = new APIRESTGetEmployeesPresenter(getEmployeesView);
        let employeeRepository = new SQLiteEmployeeRepository();
        let getEmployeesRequest = new GetEmployeesRequest(type);
        let getEmployeesUseCase = new GetEmployeesInteractor(employeeRepository, getEmployeesPresenter);

        await getEmployeesUseCase.getEmployees(getEmployeesRequest);
    }

    async generatePaymentSlipUseCase(employee) {
        let generatePaymentSlipView = new APIRESTGeneratePaymentSlipView(this._serverResponse);
        let generatePaymentSlipPresenter = new APIRESTGeneratePaymentSlipPresenter(generatePaymentSlipView);
        let paymentSlipRepository = new SQLitePaymentSlipRepository();
        let generatePaymentSlipRequest = new GeneratePaymentSlipRequest(employee);
        let generatePaymentSlipUseCase = new GeneratePaymentSlipInteractor(paymentSlipRepository, generatePaymentSlipPresenter);

        await generatePaymentSlipUseCase.generatePaymentSlip(generatePaymentSlipRequest);
    }
}

module.exports = APIRESTController;