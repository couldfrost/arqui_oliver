const path = require('path');
const GeneratePaymentSlipPresenter = require(
    path.join(process.cwd(), 'src', 'domain', 'boundary', 'output', 'generate_payment_slip_presenter')
);
const APIRESTGeneratePaymentSlipViewModel = require(
    path.join(process.cwd(), 'src', 'presentation', 'apiRest', 'viewModel', 'api_rest_generate_payment_slip_view_model')
);

class APIRESTGeneratePaymentSlipPresenter extends GeneratePaymentSlipPresenter {
    constructor(apiRestGeneratePaymentSlipView) {
        super();
        this._apiRestGeneratePaymentSlipView = apiRestGeneratePaymentSlipView;
    }

    displayGeneratedPaymentSlip(response) {
        let apiRestGeneratePaymentSlipViewModel = new APIRESTGeneratePaymentSlipViewModel(
            response.paymentSlip.employeeId,
            response.paymentSlip.employeeName,
            response.paymentSlip.employeeType,
            response.paymentSlip.salary,
        );
        this._apiRestGeneratePaymentSlipView.sendGeneratePaymentSlipResponse(apiRestGeneratePaymentSlipViewModel);
    }
}

module.exports = APIRESTGeneratePaymentSlipPresenter;