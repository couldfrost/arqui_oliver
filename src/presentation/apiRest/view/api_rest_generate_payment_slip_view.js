class APIRESTGeneratePaymentSlipView {
    constructor(serverResponse) {
        this._serverResponse = serverResponse;
    }

    sendGeneratePaymentSlipResponse(apiRestGeneratePaymentSlipViewModel) {
        this._serverResponse.json({
            employeeId: apiRestGeneratePaymentSlipViewModel.employeeId,
            employeeName: apiRestGeneratePaymentSlipViewModel.employeeName,
            employeeType: apiRestGeneratePaymentSlipViewModel.employeeType,
            salary: apiRestGeneratePaymentSlipViewModel.salary
        });
    }
}

module.exports = APIRESTGeneratePaymentSlipView;